require('mocha');
const assert = require('assert');
const isNaan = require('./');

describe('is not bread', function() {
	var cases = [
		5,
		true,
		false,
		null,
		undefined,
		0,
		'string',
		'',
		'bread'
	];
	cases.forEach(function(num, idx) {
		it(num + ' is not real bread', () => assert.equal(isNaan(num), false))
	});
});
